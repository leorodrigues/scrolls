defmodule Integration.Scrolls.PermanentScrollTest do
  use ExTestSupport.EnvironmentTools, target_app_name: :scrolls
  @moduletag :integration

  alias Mockery.Scrolls.PeopleScroll

  setup_all_environment do
    import_path = resolve_resource_path(["sample_data.terms"])
    {_, base_path} = make_session_resources_dir(["permanent_scroll_test"])
    [base_path: base_path, import_path: import_path]
  end

  setup %{base_path: p, table_name: n} do
    [table_path: Path.join(p, n)]
  end

  describe "Scrolls.PermanentScroll," do
    @tag table_name: "group1-case1.bin"
    test "should run business as usual", %{table_path: p} do
      args = [init_args: [table_path: p]]

      with_service(PeopleScroll, args, fn ->
        :ok = PeopleScroll.save({1, {"Harry", 32}})
        :ok = PeopleScroll.save({2, {"Sally", 33}})
      end)

      with_service(PeopleScroll, args, fn ->
        {"Sally", 33} = PeopleScroll.get_person_by_key(2)
        {"Harry", 32} = PeopleScroll.get_person_by_key(1)
        PeopleScroll.destroy()
      end)
    end
  end

  def with_service(service, arguments, test_function) do
    try do
      apply(service, :start_link, [arguments])
      test_function.()
    after
      apply(service, :stop, [])
    end
  end
end
