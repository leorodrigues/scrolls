defmodule Unit.Scrolls.PathsTest do
  use ExUnit.Case

  test "Scrolls.Paths.compute_table_file_path, runs just fine" do
    path = "/var/lib/my_app/data/scrolls/my_table.bin"
    ^path = Scrolls.Paths.compute_table_file_path(:my_app, :my_table)
  end
end
