defmodule Unit.Scrolls.PermanentScrollTest do
  use ExUnit.Case

  require Logger

  alias Scrolls.DummyScroll
  alias Scrolls.Backend
  alias Scrolls.ScrollState

  @fake_error {:error, "fake-error-reason"}

  setup do
    :meck.new([Backend, ScrollState])
    :meck.expect(Backend, :handle_init, fn s -> s end)
    :meck.expect(ScrollState, :from_args, fn _ -> %ScrollState{ } end)
    DummyScroll.start_link()
    on_exit(fn -> Logger.flush; :meck.unload end)
  end

  describe "Scrolls.PermanentScroll, success case of" do
    test "open scroll" do
      :meck.expect(Backend, :handle_open, fn state -> state end)

      :ok = DummyScroll.open()

      :meck.wait(Backend, :handle_open, [%ScrollState{ }], 1000)
    end

    test "close scroll" do
      :meck.expect(Backend, :handle_close, fn state -> state end)

      :ok = DummyScroll.close()

      :meck.wait(Backend, :handle_close, [%ScrollState{ }], 1000)
    end

    test "destroy scroll" do
      :meck.expect(Backend, :handle_destroy, fn state -> state end)

      :ok = DummyScroll.destroy()

      :meck.wait(Backend, :handle_destroy, [%ScrollState{ }], 1000)
    end

    test "import scroll" do
      :meck.expect(Backend, :handle_import, fn _, state -> state end)

      :ok = DummyScroll.import("somepath")

      :meck.wait(Backend, :handle_import, ["somepath", %ScrollState{ }], 1000)
    end

    test "sync scroll" do
      :meck.expect(Backend, :handle_sync, fn state -> state end)

      :ok = DummyScroll.sync()

      :meck.wait(Backend, :handle_sync, [%ScrollState{ }], 1000)
    end

    test "execute function w/o arguments" do
      :meck.expect(Backend, :handle_execute, fn _, state -> state end)

      :ok = DummyScroll.execute_fn(:fn)

      :meck.wait(Backend, :handle_execute, [{:fn, [ ]}, %ScrollState{ }], 1000)
    end

    test "execute function with arguments" do
      :meck.expect(Backend, :handle_execute, fn _, state -> state end)

      :ok = DummyScroll.execute_fn(:fn, [1, 2, 5, 7])

      :meck.wait(Backend, :handle_execute, [
        {:fn, [1, 2, 5, 7]}, %ScrollState{ }
      ], 1000)
    end

    test "execute mfa w/o arguments" do
      :meck.expect(Backend, :handle_execute, fn _, state -> state end)

      :ok = DummyScroll.execute_mfa(My.Module, :fn)

      :meck.wait(Backend, :handle_execute, [
        {My.Module, :fn, [ ]}, %ScrollState{ }
      ], 1000)
    end

    test "execute mfa with arguments" do
      :meck.expect(Backend, :handle_execute, fn _, state -> state end)

      :ok = DummyScroll.execute_mfa(My.Module, :fn, [1, 2, 5, 7])

      :meck.wait(Backend, :handle_execute, [
        {My.Module, :fn, [1, 2, 5, 7]}, %ScrollState{ }
      ], 1000)
    end
  end

  describe "Scrolls.PermanentScroll, error case of" do
    test "open scroll" do
      :meck.expect(Backend, :handle_open, fn s -> {@fake_error, s} end)

      @fake_error = DummyScroll.open()

      :meck.wait(Backend, :handle_open, [%ScrollState{ }], 1000)
    end

    test "close scroll" do
      :meck.expect(Backend, :handle_close, fn s -> {@fake_error, s} end)

      @fake_error = DummyScroll.close()

      :meck.wait(Backend, :handle_close, [%ScrollState{ }], 1000)
    end

    test "destroy scroll" do
      :meck.expect(Backend, :handle_destroy, fn s -> {@fake_error, s} end)

      @fake_error = DummyScroll.destroy()

      :meck.wait(Backend, :handle_destroy, [%ScrollState{ }], 1000)
    end

    test "import scroll" do
      :meck.expect(Backend, :handle_import, fn _, s -> {@fake_error, s} end)

      @fake_error = DummyScroll.import("somepath")

      :meck.wait(Backend, :handle_import, ["somepath", %ScrollState{ }], 1000)
    end

    test "sync scroll" do
      :meck.expect(Backend, :handle_sync, fn s -> {@fake_error, s} end)

      @fake_error = DummyScroll.sync()

      :meck.wait(Backend, :handle_sync, [%ScrollState{ }], 1000)
    end

    test "execute function w/o arguments" do
      invocation = fn _, state -> {@fake_error, state} end
      :meck.expect(Backend, :handle_execute, invocation)

      @fake_error = DummyScroll.execute_fn(:fn)

      :meck.wait(Backend, :handle_execute, [{:fn, [ ]}, %ScrollState{ }], 1000)
    end

    test "execute function with arguments" do
      invocation = fn _, state -> {@fake_error, state} end
      :meck.expect(Backend, :handle_execute, invocation)

      @fake_error = DummyScroll.execute_fn(:fn, [1, 2, 5, 7])

      :meck.wait(Backend, :handle_execute, [
        {:fn, [1, 2, 5, 7]}, %ScrollState{ }
      ], 1000)
    end

    test "execute mfa w/o arguments" do
      invocation = fn _, state -> {@fake_error, state} end
      :meck.expect(Backend, :handle_execute, invocation)

      @fake_error = DummyScroll.execute_mfa(My.Module, :fn)

      :meck.wait(Backend, :handle_execute, [
        {My.Module, :fn, [ ]}, %ScrollState{ }
      ], 1000)
    end

    test "execute mfa with arguments" do
      invocation = fn _, state -> {@fake_error, state} end
      :meck.expect(Backend, :handle_execute, invocation)

      @fake_error = DummyScroll.execute_mfa(My.Module, :fn, [1, 2, 5, 7])

      :meck.wait(Backend, :handle_execute, [
        {My.Module, :fn, [1, 2, 5, 7]}, %ScrollState{ }
      ], 1000)
    end
  end
end
