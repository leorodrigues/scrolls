defmodule Unit.Scrolls.BackendTest do
  use ExTestSupport.EnvironmentTools, target_app_name: :scrolls

  alias Scrolls.Backend

  setup_all_environment do
    import_path = resolve_resource_path(["sample_data.terms"])
    {_, base_path} = make_session_resources_dir(["backend_test"])

    [base_path: base_path, import_path: import_path]
  end

  setup %{base_path: p, table_name: n} do
    [table_path: Path.join(p, n)]
  end

  describe "Scrolls.Backend.handle_open," do
    @tag table_name: "group1-case1.bin"
    test "creates the file if it does not exist", %{table_path: p} do
      state = %{table_path: p, table_name: :fake_table}
      %{table: :fake_table, table_path: p} = Backend.handle_open(state)
      true = File.exists?(p)
      :dets.close(:fake_table)
    end

    @tag table_name: "group1-case2.bin"
    test "just open the file if it is already there", %{table_path: p} do
      true = File.exists?(p)
      state = %{table_path: p, table_name: :fake_table}
      %{table: :fake_table, table_path: ^p} = Backend.handle_open(state)
      :dets.close(:fake_table)
    end
  end

  describe "Scrolls.Backend.handle_close," do
    @tag table_name: "group7-case1.bin"
    test "if open, close the table and keep the file", %{table_path: p} do
      {:ok, t} = :dets.open_file(to_charlist(p))
      expected = %{table_path: p}
      ^expected = Backend.handle_close(%{table_path: p, table: t})
      true = File.exists?(p)
    end

    @tag table_name: "group7-case2.bin"
    test "if already closed, do nothing", %{table_path: p} do
      expected = %{table_path: p}
      ^expected = Backend.handle_close(%{table_path: p})
      true = File.exists?(p)
    end
  end

  describe "Scrolls.Backend.handle_destroy," do
    @tag table_name: "group2-case1.bin"
    test "close the table and removes the existing file", %{table_path: p} do
      {:ok, t} = :dets.open_file(to_charlist(p))
      state = %{table_path: p, table_name: :fake_table, table: t}
      %{table_path: ^p, table_name: :fake_table} = Backend.handle_destroy(state)
      false = File.exists?(p)
    end

    @tag table_name: "group2-case2.bin"
    test "just removes the existing file", %{table_path: p} do
      true = File.exists?(p)
      %{table_path: ^p} = Backend.handle_destroy(%{table_path: p})
      false = File.exists?(p)
    end

    @tag table_name: "group2-case3.bin"
    test "does nothing if there is no file", %{table_path: p} do
      false = File.exists?(p)
      %{table_path: ^p} = Backend.handle_destroy(%{table_path: p})
      false = File.exists?(p)
    end
  end

  describe "Scrolls.Backend.handle_import," do
    @tag table_name: "group3-case1.bin"
    test "runs just fine", %{table_path: p, import_path: i} do
      state = %{table_path: p, table_name: :fake_table}
      %{table_path: ^p, table: t} = Backend.handle_import(i, state)
      [{1, {:some_atom, 'hello'}}] = :dets.lookup(t, 1)
    end

    @tag table_name: "group3-case2.bin"
    test "fails if the terms file does not exist", %{table_path: p} do
      state = %{table_path: p, table: :fake_table}
      expected = {{:error, {:file_not_found, "fake-path"}}, state}
      ^expected = Backend.handle_import("fake-path", state)
    end

    @tag table_name: "group3-case3.bin"
    test "fails if the table file does not exist", context do
      %{table_path: p, import_path: i} = context
      state = %{table_path: p, table_name: :fake_table}
      expected = {{:error, {:table_not_found, p}}, state}
      ^expected = Backend.handle_import(i, state)
    end
  end

  describe "Scrolls.Backend.handle_sync," do
    @tag table_name: "group4-case1.bin"
    test "sync the table if open", context do
      %{table_path: p} = context
      {:ok, t} = :dets.open_file(to_charlist(p))
      %{table_path: ^p} = Backend.handle_sync(%{table_path: p, table: t})
      :dets.close(t)
    end

    @tag table_name: "group4-case2.bin"
    test "does nothing if there is no table", context do
      %{table_path: p} = context
      %{table_path: ^p} = Backend.handle_sync(%{table_path: p})
    end
  end

  describe "Scrolls.Backend.handle_execute," do
    @tag table_name: "empty-table.bin"
    test "invoke a function w/o arguments just fine", context do
      %{table_path: p} = context
      function = fn t -> {112358, t} end
      state = %{table_path: p, table_name: :tbl}
      expected = {{112358, :tbl}, %{table_path: p, table_name: :tbl, table: :tbl}}
      ^expected = Backend.handle_execute({function, [ ]}, state)
    end

    @tag table_name: "empty-table.bin"
    test "invoke a function with arguments just fine", context do
      %{table_path: p} = context
      function = fn t, a, b -> {132134, t, a, b} end
      state = %{table_path: p, table_name: :tbl}
      expected = {{132134, :tbl, 5, 7}, %{table_path: p, table_name: :tbl, table: :tbl}}
      ^expected = Backend.handle_execute({function, [5, 7]}, state)
    end

    @tag table_name: "empty-table.bin"
    test "invoke an MF w/o arguments just fine", %{table_path: p} do
      run_and_cleanup(fn ->
        :meck.new(My.Module, [:non_strict])
        :meck.expect(My.Module, :my_func, fn t -> {5589, t} end)

        state = %{table_path: p, table_name: :tbl}
        expected = {{5589, :tbl}, %{table_path: p, table_name: :tbl, table: :tbl}}
        ^expected = Backend.handle_execute({My.Module, :my_func, [ ]}, state)
      end)
    end

    @tag table_name: "empty-table.bin"
    test "invoke an MFA just fine", %{table_path: p} do
      run_and_cleanup(fn ->
        :meck.new(My.Module, [:non_strict])
        :meck.expect(My.Module, :my_func, fn t, a, b -> {5589, t, a, b} end)

        state = %{table_path: p, table_name: :tbl}
        expected = {{5589, :tbl, 7, 11}, %{table_path: p, table_name: :tbl, table: :tbl}}
        ^expected = Backend.handle_execute({My.Module, :my_func, [7, 11]}, state)
      end)
    end
  end

  describe "Scrolls.Backend.handle_down," do
    @tag table_name: "empty-table.bin"
    test "runs just fine if the table is there", %{table_path: p} do
      {:ok, t} = :dets.open_file(to_charlist(p))
      :read_write = :dets.info(t, :access)

      state = %{table_path: p, table: t}
      ^state = Backend.handle_down(state)

      :undefined = :dets.info(t, :access)
    end

    @tag table_name: "empty-table.bin"
    test "runs just fine if the table is not there", %{table_path: p} do
      state = %{table_path: p}
      ^state = Backend.handle_down(state)
    end
  end

  describe "Scrolls.Backend.handle_init," do
    @tag table_name: "group8-case1.bin"
    test "creates the file", %{table_path: p} do
      state = %{table_path: p, table_name: :fake_table}
      %{table: :fake_table} = Backend.handle_init(state)
      true = File.exists?(p)
      :dets.close(:fake_table)
    end

    @tag table_name: "group8-case2.bin"
    test "does not create the file", %{table_path: p} do
      state = %{skip_open_at_start: true}
      ^state = Backend.handle_init(state)
      false = File.exists?(p)
    end
  end

  def run_and_cleanup(test) do
    try do
      test.()
    after
      :meck.unload
    end
  end
end
