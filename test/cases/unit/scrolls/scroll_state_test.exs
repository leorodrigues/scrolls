defmodule Unit.Scrolls.ScrollStateTest do
  use ExUnit.Case

  alias Scrolls.ScrollState, as: State
  alias Scrolls.Paths

  setup do
    :meck.new(Paths)
    on_exit(fn -> :meck.unload end)
  end

  describe "Scrolls.ScrollState.from_args," do
    test "sets required values only" do
      :meck.expect(Paths, :compute_table_file_path, fn _, _ -> "fake_path" end)
      args = [table_name: :my_table, target_app_name: :my_app]
      stat = %State{
        skip_open_at_start: false,
        table_name: :my_table,
        table_path: "fake_path"
      }

      ^stat = State.from_args(args)

      :meck.wait(Paths, :compute_table_file_path, [:my_app, :my_table], 100)
    end

    test "sets required and optional values" do
      :meck.expect(Paths, :compute_table_file_path, fn _, _ -> "fake_path" end)
      args = [
        table_name: :my_table,
        target_app_name: :my_app,
        skip_open_at_start: true
      ]
      stat = %State{
        skip_open_at_start: true,
        table_name: :my_table,
        table_path: "fake_path"
      }

      ^stat = State.from_args(args)

      :meck.wait(Paths, :compute_table_file_path, [:my_app, :my_table], 100)
    end

    test "allows overriding of computed table path" do
      :meck.expect(Paths, :compute_table_file_path, fn _, _ -> "fake_path" end)
      args = [
        table_name: :my_table,
        target_app_name: :my_app,
        skip_open_at_start: true,
        table_path: "overriding-path"
      ]
      stat = %State{
        skip_open_at_start: true,
        table_name: :my_table,
        table_path: "overriding-path"
      }

      ^stat = State.from_args(args)

      :meck.wait(Paths, :compute_table_file_path, [:my_app, :my_table], 100)
    end

    test "complains about missing table name" do
      assert_raise(KeyError, fn -> State.from_args([ ]) end)
    end

    test "complains about missing target app name" do
      assert_raise(KeyError, fn -> State.from_args([table_name: :my_table]) end)
    end
  end
end
