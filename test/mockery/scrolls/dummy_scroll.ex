defmodule Scrolls.DummyScroll do
  use Scrolls.PermanentScroll, scroll_name: :dummy_scroll

  def find_by_key(key) do
    execute_fn(fn table -> :dets.lookup(table, key) end)
  end
end
