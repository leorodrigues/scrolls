defmodule Mockery.Scrolls.PeopleScroll do
  use Scrolls.PermanentScroll, scroll_name: :people

  def save(person) do
    execute_fn(fn table -> :dets.insert(table, person) end)
  end

  def get_person_by_key(key) do
    execute_fn(fn table -> :dets.lookup(table, key) end)
      |> only_first()
      |> only_content()
  end

  defp only_first([e|_]), do: e
  defp only_first([ ]), do: nil

  defp only_content({_, content}), do: content
end
