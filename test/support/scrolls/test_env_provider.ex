defmodule Scrolls.TestEnvProvider do
  def get_env(app_name, :scrolls_base_data_dir = key, default) do
    case ExTestSupport.Utilities.make_session_resources_dir(app_name, ["scrolls_data"]) do
      {:error, _} -> Application.get_env(app_name, key, default)
      {_, path} -> path
    end
  end

  def get_env(app_name, key, nil) do
    Application.get_env(app_name, key)
  end

  def get_env(app_name, key, default) do
    Application.get_env(app_name, key, default)
  end
end
