# Scrolls

This is a small life cycle control server for Disc ETS tables. It enables the
quick development of applications that use DETS to store soft state data.

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `scrolls` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:scrolls, "~> 1.0.0"}
  ]
end
```

To install from source use this:

```elixir
def deps do
  [
    {:scrolls, git: "http://bitbucket.org/leorodrigues/scrolls.git", tag: "1.0.0"}
  ]
end
```

## Usage

First Decleare a "Scroll" like so:

```elixir
defmodule MyApp.PeopleScroll do
  use Scrolls.PermanentScroll, scroll_name: :people

  def find_by_key(key) do
    execute_fn(fn table ->
      case :dets.lookup(table, key) do
        [ ] -> :not_found
        [person] -> person
      end
    end)
  end

  def set(key, person) do
    execute_fn(fn table -> :dets.insert(table, {key, person}) end)
  end
end
```

Then make sure the "Scroll" is initialized:

```elixir
defmodule MyApp.Application do
  use Application

  @impl true
  def start(_type, _args) do
    children = [
      {MyApp.PeopleScroll, [ ]}
    ]

    opts = [strategy: :one_for_one, name: Scrolls.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
```

Finally, use the scroll anywhere in your application:

```elixir
defmodule MyApp.People.UseCases do
  def update_person(key, person_data) do
    case MyApp.PeopleScroll.find_by_key(key) do
      :not_found -> :not_found
      person -> MyApp.PeopleScroll.set(key, merge_person(person, person_data))
  end
end
```

## Configuration

Each scroll has the following configuration keys:

 - :table_name - Overrides the :scroll_name given in the use clause of the scroll module
 - :skip_open_at_start - Stop the scroll from loadding right at the app initialization
 - :table_path - Override the computed file path for the table

Those variables must be given at the supervisor child configuration call:


```elixir
defmodule MyApp.Application do
  use Application

  @impl true
  def start(_type, _args) do
    children = [
      {MyApp.PeopleScroll, [
      	init_args: [
	  table_name: :my_table,
	  skip_open_at_start: true,
	  table_path: "/some/path/to/table"
	]
      ]}
    ]

    opts = [strategy: :one_for_one, name: Scrolls.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
```
