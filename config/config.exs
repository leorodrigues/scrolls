import Config

config :logger, :console,
  format: "$time $node $metadata[$level] $message\n",
  metadata: [:node, :module, :pid, :request_id]

import_config "#{config_env()}.exs"

