import Config

config :scrolls,
  env_provider: Scrolls.TestEnvProvider

config :logger,
  backends: [:console]
