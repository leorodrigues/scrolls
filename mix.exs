defmodule Scrolls.MixProject do
  use Mix.Project

  def project do
    [
      app: :scrolls,
      version: "1.1.9",
      config_path: "config/config.exs",
      elixir: "~> 1.15",
      elixirc_paths: elixirc_paths(Mix.env()),
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      test_coverage: [tool: ExCoveralls],
      aliases: aliases(),
      preferred_cli_env: [
        coveralls: :test,
        "coveralls.html": :test,
        integration: :test,
        unit: :test
      ]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {Scrolls.Application, []}
    ]
  end

  defp elixirc_paths(:test), do: ["lib", "test/support", "test/mockery"]
  defp elixirc_paths(:dev), do: ["lib", "test/mockery"]
  defp elixirc_paths(_), do: ["lib"]

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:meck, "~> 0.9.2", only: [:test], runtime: false},
      {:dialyxir, "~> 1.3", only: [:dev], runtime: false},
      {:excoveralls, "~> 0.14.4", only: [:test]},
      {:church, git: "http://bitbucket.org/leorodrigues/erlang-church.git", tag: "2.0.1"},
      {:ex_test_support, only: [:test], git: "http://bitbucket.org/leorodrigues/ex-test-support.git", tag: "2.0.0"}
    ]
  end

  defp aliases do
    [
      integration: ["test --only integration:true"],
      unit: ["test --exclude integration:true"]
    ]
  end
end
