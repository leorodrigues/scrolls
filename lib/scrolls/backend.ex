defmodule Scrolls.Backend do

  alias Scrolls.ScrollState

  require Logger

  import :church, only: [chain_apply: 2, partial_left: 2]

  @type mfa_arg_list :: {module(), atom(), list(any)}

  @type func_arg_list :: {function() | reference(), list(any)}

  @type handler_return :: ScrollState.t() | {{:error, term()}, ScrollState.t()}

  @create_path_ok "Created base path."

  @create_path_error "Failed to create base path."

  @spec handle_open(state :: ScrollState.t()) :: handler_return()
  def handle_open(state) do
    chain_apply(state, [
      &ensure_base_path_exists/1,
      &open_dets_table/1,
      partial_left(&add_table_to_state/2, [state])
    ])
  end

  @spec handle_close(state :: ScrollState.t()) :: handler_return()
  def handle_close(state) do
    chain_apply(state, [
      &try_closing_table/1,
      &remove_table_from_state/1
    ])
  end

  @spec handle_destroy(state :: ScrollState.t()) :: handler_return()
  def handle_destroy(state) do
    chain_apply(state, [
      &handle_close/1,
      &break_when_table_file_missing/1,
      &remove_table_file/1,
    ])
  end

  @spec handle_import(
    file_path :: String.t(),
    state :: ScrollState.t()) :: handler_return()
  def handle_import(file_path, state) do
    chain_apply(state, [
      &try_loading_table/1,
      &break_on_error/1,
      partial_left(&import_table/2, [file_path])
    ])
  end

  @spec handle_sync(state :: ScrollState.t()) :: handler_return()
  def handle_sync(state) do
    chain_apply(state, [
      &try_expanding_table/1,
      partial_left(&sync_table/2, [state])
    ])
  end

  @spec handle_execute(
    args :: mfa_arg_list(),
    state :: ScrollState.t()) :: {any(), ScrollState.t()}
  def handle_execute({module, function, arguments}, state) do
    chain_apply(state, [
      &try_loading_table/1,
      &break_on_error/1,
      partial_left(&invoke_mfa/4, [module, function, arguments]),
    ])
  end

  @spec handle_execute(
    args :: func_arg_list(),
    state :: ScrollState.t()) :: {any(), ScrollState.t()}
  def handle_execute({function, arguments}, state) do
    chain_apply(state, [
      &try_loading_table/1,
      &break_on_error/1,
      partial_left(&invoke_function/3, [function, arguments]),
    ])
  end

  @spec handle_down(state :: ScrollState.t()) :: handler_return()
  def handle_down(state) do
    chain_apply(state, [
      &try_expanding_table/1,
      partial_left(&sync_table/2, [state]),
      &close_table/1
    ])
  end

  @spec handle_init(state :: ScrollState.t()) :: ScrollState.t()
  def handle_init(state) do
    ensure_table_open(state)
  end

  defp ensure_table_open(%{skip_open_at_start: true} = state), do: state
  defp ensure_table_open(state), do: handle_open(state)

  defp break_when_table_file_missing(%{table_path: path} = state) do
    case File.exists?(path) do
      true -> state
      false -> {:break, state}
    end
  end

  defp try_closing_table(%{table: t} = state) do
    case :dets.close(t) do
      {:error, _} = e -> {:break, {e, state}}
      :ok -> state
    end
  end

  defp try_closing_table(state), do: state

  defp try_expanding_table(%{table: table}), do: table

  defp try_expanding_table(state), do: {:break, state}

  defp try_loading_table(%{table: _} = state), do: state

  defp try_loading_table(state) do
    chain_apply(state, [
      &confirm_table_exists/1,
      &open_dets_table/1,
      partial_left(&add_table_to_state/2, [state])
    ])
  end

  defp break_on_error({{:error, _}, _} = e), do: {:break, e}

  defp break_on_error(outcome), do: outcome

  defp sync_table(table, state) do
    case :dets.sync(table) do
      {:error, _} = e -> {e, state}
      :ok -> state
    end
  end

  defp close_table(%{table: t} = state) do
    :dets.close(t)
    state
  end

  defp invoke_mfa(%{table: t} = state, module, function, arguments) do
    {apply(module, function, Enum.concat([t], arguments)), state}
  end

  defp invoke_function(%{table: t} = state, function, arguments) do
    {apply(function, Enum.concat([t], arguments)), state}
  end

  defp import_table(state, file_path) do
    chain_apply(file_path, [
      partial_left(&confirm_terms_file_exists/2, [state]),
      &consult_terms_file/1,
      partial_left(&save_terms/2, [state])
    ])
  end

  defp confirm_terms_file_exists(file_path, state) do
    case File.exists?(file_path) do
      false -> {:break, {{:error, {:file_not_found, file_path}}, state}}
      true -> file_path
    end
  end

  defp save_terms(terms, %{table: t} = state) do
    case :dets.insert(t, terms) do
      {:error, _} = e -> {e, state}
      :ok -> state
    end
  end

  defp consult_terms_file(file_path) do
    {:ok, terms} = :file.consult(to_charlist(file_path))
    terms
  end

  defp add_table_to_state(table, state) do
    Map.put(state, :table, table)
  end

  defp open_dets_table(%{table_path: path, table_name: name}) do
    {:ok, table} = :dets.open_file(name, [
      {:type, :set},
      {:file, to_charlist(path)}
    ])
    table
  end

  defp confirm_table_exists(%{table_path: path} = state) do
    case File.exists?(path) do
      false -> {:break, {{:error, {:table_not_found, path}}, state}}
      true -> state
    end
  end

  defp remove_table_file(%{table_path: path} = state) do
    File.rm!(path); state
  end

  defp remove_table_from_state(state) do
    Map.drop(state, [:table])
  end

  defp ensure_base_path_exists(%{table_path: p} = state) do
    ensure_base_path_exists(Path.dirname(p))
    state
  end

  defp ensure_base_path_exists(p) do
    case File.mkdir_p(p) do
      :ok -> Logger.debug(msg: @create_path_ok, path: p)
      {:error, r} ->
        Logger.error(msg: @create_path_error, path: p, reason: r)
        raise RuntimeError, "Failed to create path. path: #{p}, reason: #{r}"
    end
  end
end
