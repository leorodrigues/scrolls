defmodule Scrolls.EnvProvider do
  def get_env(app_name, key, default \\ nil) do
    Application.get_env(app_name, key, default)
  end
end
