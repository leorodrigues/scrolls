defmodule Scrolls.ScrollState do
  alias Scrolls.ScrollState

  import Scrolls.Paths, only: [compute_table_file_path: 2]
  import Keyword, only: [fetch!: 2]

  defstruct [
    :table_path,
    :table_name,
    skip_open_at_start: false
  ]

  @type t :: %__MODULE__{
    table_path: String.t(),
    table_name: atom(),
    skip_open_at_start: boolean()
  }

  def from_args(arguments \\ [ ]) do
    %ScrollState{ }
    |> set_argument(:table_name, arguments)
    |> set_argument(:skip_open_at_start, arguments, false)
    |> set_argument(:table_path, arguments, resolve_table_path!(arguments))
  end

  defp set_argument(%ScrollState{ } = state, key, arguments, default \\ nil) do
    case Keyword.get(arguments, key, default) do
      nil -> state
      value -> Map.put(state, key, value)
    end
  end

  defp resolve_table_path!(args) do
    table_name = fetch!(args, :table_name)
    target_app_name = fetch!(args, :target_app_name)
    compute_table_file_path(target_app_name, table_name)
  end
end
