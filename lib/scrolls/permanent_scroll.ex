defmodule Scrolls.PermanentScroll do
  defmacro __using__(opts) do
    unless :scroll_name in Keyword.keys(opts) do
      raise ArgumentError, "The :scroll_name parameter is required."
    end

    quote location: :keep, bind_quoted: [opts: opts] do
      use GenServer

      require Logger

      alias Scrolls.ScrollState, as: State

      import Keyword, only: [put: 3]
      import Enum, only: [into: 2]
      import Scrolls.Backend, only: [
        handle_open: 1,
        handle_close: 1,
        handle_destroy: 1,
        handle_import: 2,
        handle_sync: 1,
        handle_execute: 2,
        handle_down: 1,
        handle_init: 1
      ]

      @myself __MODULE__
      @scroll_name Keyword.get(opts, :scroll_name)

      def self, do: @myself

      def open do
        GenServer.call(@myself, :open)
      end

      def close do
        GenServer.call(@myself, :close)
      end

      def destroy do
        GenServer.call(@myself, :destroy)
      end

      def import(file_path) do
        GenServer.call(@myself, {:import, file_path})
      end

      def sync do
        GenServer.call(@myself, :sync)
      end

      def execute_fn(function, arguments \\ [ ]) do
        GenServer.call(@myself, {:execute, {function, arguments}})
      end

      def execute_mfa(module, function, arguments \\ [ ]) do
        GenServer.call(@myself, {:execute, {module, function, arguments}})
      end

      def start_link(opts \\ [ ]) do
        Logger.debug([msg: "Starting link.", scroll_name: @scroll_name])
        args = Keyword.get(opts, :init_args, [ ])
        GenServer.start_link(__MODULE__, args, name: @myself)
      end

      def stop do
        Logger.debug([msg: "Stoping link.", scroll_name: @scroll_name])
        GenServer.stop(@myself)
      end

      @impl true
      def init(args \\ [ ]) do
        {:ok, add_local_values(args) |> State.from_args() |> handle_init()}
      end

      @impl true
      def handle_call({:import, file_path}, _from, state) do
        handle_import(file_path, state) |> reply_call()
      end

      @impl true
      def handle_call(:open, _from, state) do
        handle_open(state) |> reply_call()
      end

      @impl true
      def handle_call(:close, _from, state) do
        handle_close(state) |> reply_call()
      end

      @impl true
      def handle_call(:destroy, _from, state) do
        handle_destroy(state) |> reply_call()
      end

      @impl true
      def handle_call(:sync, _from, state) do
        handle_sync(state) |> reply_call()
      end

      @impl true
      def handle_call({:execute, parameters}, _from, state) do
        handle_execute(parameters, state) |> reply_call()
      end

      @impl true
      def terminate(_reason, state) do
        Logger.debug([msg: "Terminating.", scroll_name: @scroll_name])
        state |> handle_sync() |> handle_close()
        :ok
      end

      @impl true
      def handle_info({:DOWN, _, _, _, _}, state) do
        Logger.debug([msg: "Server going down.", scroll_name: @scroll_name])
        handle_down(state) |> reply_info()
      end

      defp reply_info({{:error, reason}, state}) do
        Logger.error([
          msg: "Fatal error", scroll_name: @scroll_name, reason: reason
        ])
        {:noreply, state}
      end

      defp reply_info(state) do
        {:noreply, state}
      end

      defp reply_call({{:error, _} = e, state}) do
        {:reply, e, state}
      end

      defp reply_call({result, state}) do
        {:reply, result, state}
      end

      defp reply_call(%{ } = state) do
        {:reply, :ok, state}
      end

      defp add_local_values(arguments) do
        arguments
          |> put(:table_name, @scroll_name)
          |> put(:target_app_name, Application.get_application(__MODULE__))
      end
    end
  end
end
