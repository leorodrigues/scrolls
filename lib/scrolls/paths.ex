defmodule Scrolls.Paths do

  alias Scrolls.EnvProvider

  @default_base_dir "/var/lib"

  @base_dir_key :scrolls_base_data_dir

  @env_provider Application.compile_env(:scrolls, :env_provider, EnvProvider)

  def compute_table_file_path(target_app_name, table_name) do
    Path.join([
      get_app_base_data_path(target_app_name),
      to_string(target_app_name),
      "data",
      "scrolls",
      "#{to_string(table_name)}.bin"
    ])
  end

  defp get_app_base_data_path(target_app_name) do
    @env_provider.get_env(target_app_name, @base_dir_key, @default_base_dir)
  end
end
